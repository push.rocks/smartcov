"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const plugins = require("./smartcov.plugins");
exports.percentageFromLcovString = (fileStringArg, roundedTo, format) => {
    let done = plugins.q.defer();
    let report = fileStringArg;
    if (report === '') {
        console.log('can\'t parse empty string');
        done.reject('can\'t parse empty string');
        return;
    }
    let coveragePercentage;
    let finalCoverage;
    format = format || 'lcov';
    switch (format) {
        case 'jscoverage':
            plugins.beautylog.warn('jscoverage not yet implemented');
            done.reject('error');
            break;
        case 'lcov':
        default:
            plugins.lcovParse(report, (err, data) => {
                if (err) {
                    console.log(err);
                }
                ;
                let hit = 0;
                let found = 0;
                for (let i = 0; i < data.length; i++) {
                    hit += data[i].lines.hit;
                    found += data[i].lines.found;
                }
                coveragePercentage = (hit / found) * 100;
                finalCoverage = plugins.mathjs.round(coveragePercentage, roundedTo);
                done.resolve(finalCoverage);
            });
            break;
    }
    return done.promise;
};
exports.percentageFromLcovFile = (filePathArg, roundedToArg, formatArg) => __awaiter(this, void 0, void 0, function* () {
    let report;
    try {
        report = plugins.fs.readFileSync(filePathArg, 'utf8');
    }
    catch (err) {
        plugins.beautylog.warn('no file found at ' + filePathArg);
        throw new Error('no file found at ' + filePathArg);
    }
    return yield exports.percentageFromLcovString(report, roundedToArg, formatArg);
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic21hcnRjb3YuZ2V0LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vdHMvc21hcnRjb3YuZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSw4Q0FBOEM7QUFFbkMsUUFBQSx3QkFBd0IsR0FBRyxDQUFDLGFBQXFCLEVBQUUsU0FBaUIsRUFBRSxNQUFlO0lBQzlGLElBQUksSUFBSSxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFVLENBQUE7SUFDcEMsSUFBSSxNQUFNLEdBQUcsYUFBYSxDQUFBO0lBQzFCLEVBQUUsQ0FBQyxDQUFDLE1BQU0sS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsMkJBQTJCLENBQUMsQ0FBQTtRQUN4QyxJQUFJLENBQUMsTUFBTSxDQUFDLDJCQUEyQixDQUFDLENBQUE7UUFDeEMsTUFBTSxDQUFBO0lBQ1IsQ0FBQztJQUNELElBQUksa0JBQTBCLENBQUE7SUFDOUIsSUFBSSxhQUFxQixDQUFBO0lBQ3pCLE1BQU0sR0FBRyxNQUFNLElBQUksTUFBTSxDQUFBO0lBQ3pCLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7UUFDZixLQUFLLFlBQVk7WUFDZixPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFBO1lBQ3hELElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUE7WUFDcEIsS0FBSyxDQUFBO1FBQ1AsS0FBSyxNQUFNLENBQUM7UUFDWjtZQUNFLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxFQUFFLElBQUk7Z0JBQ2xDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQ1IsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQTtnQkFDbEIsQ0FBQztnQkFBQSxDQUFDO2dCQUNGLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQTtnQkFDWCxJQUFJLEtBQUssR0FBRyxDQUFDLENBQUE7Z0JBQ2IsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7b0JBQ3JDLEdBQUcsSUFBSSxJQUFJLENBQUUsQ0FBQyxDQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQTtvQkFDMUIsS0FBSyxJQUFJLElBQUksQ0FBRSxDQUFDLENBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFBO2dCQUNoQyxDQUFDO2dCQUNELGtCQUFrQixHQUFHLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxHQUFHLEdBQUcsQ0FBQTtnQkFDeEMsYUFBYSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGtCQUFrQixFQUFFLFNBQVMsQ0FBQyxDQUFBO2dCQUNuRSxJQUFJLENBQUMsT0FBTyxDQUFDLGFBQWEsQ0FBQyxDQUFBO1lBQzdCLENBQUMsQ0FBQyxDQUFBO1lBQ0YsS0FBSyxDQUFBO0lBQ1QsQ0FBQztJQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFBO0FBQ3JCLENBQUMsQ0FBQTtBQUVVLFFBQUEsc0JBQXNCLEdBQUcsQ0FBTyxXQUFtQixFQUFFLFlBQW9CLEVBQUUsU0FBa0I7SUFDdEcsSUFBSSxNQUFjLENBQUE7SUFDbEIsSUFBSSxDQUFDO1FBQ0gsTUFBTSxHQUFHLE9BQU8sQ0FBQyxFQUFFLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxNQUFNLENBQUMsQ0FBQTtJQUN2RCxDQUFDO0lBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUNiLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLFdBQVcsQ0FBQyxDQUFBO1FBQ3pELE1BQU0sSUFBSSxLQUFLLENBQUMsbUJBQW1CLEdBQUcsV0FBVyxDQUFDLENBQUE7SUFDcEQsQ0FBQztJQUNELE1BQU0sQ0FBQyxNQUFNLGdDQUF3QixDQUFDLE1BQU0sRUFBRSxZQUFZLEVBQUUsU0FBUyxDQUFDLENBQUE7QUFDeEUsQ0FBQyxDQUFBLENBQUEifQ==