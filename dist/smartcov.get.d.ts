export declare let percentageFromLcovString: (fileStringArg: string, roundedTo: number, format?: string) => Promise<number>;
export declare let percentageFromLcovFile: (filePathArg: string, roundedToArg: number, formatArg?: string) => Promise<number>;
