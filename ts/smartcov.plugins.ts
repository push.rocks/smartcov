import 'typings-global'
import beautylog = require('beautylog')
let fs = require('fs-extra')
let lcovParse = require('lcov-parse')
let mathjs = require('mathjs')
import q = require('smartq')

export {
  beautylog,
  fs,
  lcovParse,
  mathjs,
  q
}
