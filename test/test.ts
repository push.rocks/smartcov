import { expect, tap } from 'tapbundle'

import * as smartcov from '../dist/index.js'

tap.test('should get resolved with correct value for valid lcov.info file', async () => {
  return await expect(smartcov.get.percentageFromLcovFile('./test/lcov.info', 2)).to.eventually.equal(63.64)
}).catch(tap.threw)

tap.test('should get rejected for invalid file and warn', async () => {
  return await expect(smartcov.get.percentageFromLcovFile('./test/lcovNotThere.info', 2)).to.eventually.be.rejected
}).catch(tap.threw)
